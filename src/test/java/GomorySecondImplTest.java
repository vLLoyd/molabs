import model.Function;
import model.Restriction;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class GomorySecondImplTest {

    private GomorySecondImpl gomorySecond = new GomorySecondImpl();

    @Test
    public void run() {
        gomorySecond.run(Collections.singletonList(new Function(2.0f, 1.0f, -6.0f, "max")),
                Arrays.asList(new Restriction(-5.0f, 5.2f, "<=", 11.96f),
                        new Restriction(1.2f, 5.0f, "<=", 42.74f),
                        new Restriction(6.1f, 5.1f, "<=", 93.33f))
        );

    }
}