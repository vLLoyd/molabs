package service;

import model.Mode;
import model.SimplexTable;
import org.junit.*;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.*;

public class SimplexMethodServiceTest {

    private SimplexTable testSimplexTable;
    private SimplexMethodService simplexMethodService;

    @Test
    public void resolveSimplexTable() {
        testSimplexTable = new SimplexTable();
        testSimplexTable.setFreeElements(Arrays.asList("x1", "x2", "x3"));
        testSimplexTable.setBasis(Arrays.asList("x4", "x5", "x6"));
        testSimplexTable.setMatrix(new BigDecimal[][]{
                new BigDecimal[]{BigDecimal.valueOf(180.0), BigDecimal.valueOf(4.0), BigDecimal.valueOf(2.0), BigDecimal.valueOf(1.0)},
                new BigDecimal[]{BigDecimal.valueOf(210.0), BigDecimal.valueOf(3.0), BigDecimal.valueOf(1.0), BigDecimal.valueOf(3.0)},
                new BigDecimal[]{BigDecimal.valueOf(244.0), BigDecimal.valueOf(1.0), BigDecimal.valueOf(2.0), BigDecimal.valueOf(5.0)},
                new BigDecimal[]{BigDecimal.valueOf(0.0), BigDecimal.valueOf(-10.0), BigDecimal.valueOf(-14.0), BigDecimal.valueOf(-12.0)}
        });
        testSimplexTable.setMode(Mode.MAX);

        simplexMethodService = new SimplexMethodService(testSimplexTable);

        while (!simplexMethodService.isResolved()) {
            simplexMethodService.resolveSimplexTable(false);
        }
    }

    @Test
    public void resolveDualSimplexTable() {
        testSimplexTable = new SimplexTable();
        testSimplexTable.setFreeElements(Arrays.asList("x1", "x2", "x3"));
        testSimplexTable.setBasis(Arrays.asList("x4", "x5"));
        testSimplexTable.setMatrix(new BigDecimal[][]{
                new BigDecimal[]{BigDecimal.valueOf(-10.0), BigDecimal.valueOf(-1.0), BigDecimal.valueOf(-1.0), BigDecimal.valueOf(0.0)},
                new BigDecimal[]{BigDecimal.valueOf(8.0), BigDecimal.valueOf(2.0), BigDecimal.valueOf(1.0), BigDecimal.valueOf(-1.0)},
                new BigDecimal[]{BigDecimal.valueOf(0.0), BigDecimal.valueOf(-4.0), BigDecimal.valueOf(-2.0), BigDecimal.valueOf(-1.0)}
        });
        testSimplexTable.setMode(Mode.MIN);

        simplexMethodService = new SimplexMethodService(testSimplexTable);

        while (!simplexMethodService.isResolved()) {
            simplexMethodService.resolveSimplexTable(false);
        }
    }
}