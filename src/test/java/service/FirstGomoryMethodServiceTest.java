package service;

import model.Mode;
import model.SimplexTable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.*;

public class FirstGomoryMethodServiceTest {

    private SimplexTable testSimplexTable;
    private SimplexMethodService simplexMethodService;
    private FirstGomoryMethodService firstGomoryMethodService;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void createAndAddNewRestriction() {
        testSimplexTable = new SimplexTable();
        testSimplexTable.setFreeElements(Arrays.asList("x1", "x2"));
        testSimplexTable.setBasis(Arrays.asList("x3", "x4", "x5"));
        testSimplexTable.setMatrix(new BigDecimal[][]{
                new BigDecimal[]{BigDecimal.valueOf(-29.26), BigDecimal.valueOf(-7.7), BigDecimal.valueOf(-3.8)},
                new BigDecimal[]{BigDecimal.valueOf(59.29), BigDecimal.valueOf(-2.6), BigDecimal.valueOf(7.7)},
                new BigDecimal[]{BigDecimal.valueOf(116.39), BigDecimal.valueOf(10.3), BigDecimal.valueOf(3.6)},
                new BigDecimal[]{BigDecimal.valueOf(5.0), BigDecimal.valueOf(-1.0), BigDecimal.valueOf(-3.0)}
        });
        testSimplexTable.setMode(Mode.MAX);

        simplexMethodService = new SimplexMethodService(testSimplexTable);

        while (!simplexMethodService.isResolved()) {
            simplexMethodService.resolveSimplexTable(false);
        }

        firstGomoryMethodService = new FirstGomoryMethodService(testSimplexTable, Arrays.asList("x1", "x2"));

        int counter = 0;
        while (!firstGomoryMethodService.isResolved()) {
            firstGomoryMethodService.resolveFirstGomory();
            counter++;
            if (counter == 50)
                break;
        }
    }

    @Test
    public void createAndAddNewRestriction2() {
        testSimplexTable = new SimplexTable();
        testSimplexTable.setFreeElements(Arrays.asList("x1", "x2"));
        testSimplexTable.setBasis(Arrays.asList("x3", "x4", "x5"));
        testSimplexTable.setMatrix(new BigDecimal[][]{
                new BigDecimal[]{BigDecimal.valueOf(11.96), BigDecimal.valueOf(-5.0), BigDecimal.valueOf(5.2)},
                new BigDecimal[]{BigDecimal.valueOf(42.74), BigDecimal.valueOf(1.2), BigDecimal.valueOf(5.0)},
                new BigDecimal[]{BigDecimal.valueOf(93.33), BigDecimal.valueOf(6.1), BigDecimal.valueOf(5.1)},
                new BigDecimal[]{BigDecimal.valueOf(6.0), BigDecimal.valueOf(-2.0), BigDecimal.valueOf(-1.0)}
        });
        testSimplexTable.setMode(Mode.MAX);

        simplexMethodService = new SimplexMethodService(testSimplexTable);

        while (!simplexMethodService.isResolved()) {
            simplexMethodService.resolveSimplexTable(false);
        }

        firstGomoryMethodService = new FirstGomoryMethodService(testSimplexTable, Arrays.asList("x1", "x2"));

        int counter = 0;
        while (!firstGomoryMethodService.isResolved()) {
            firstGomoryMethodService.resolveFirstGomory();
            counter++;
            if (counter == 50)
                break;
        }
    }
}