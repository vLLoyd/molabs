import model.Function;
import model.Restriction;
import util.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class GomorySecondImpl
{
    private float[][] table;
    private float[][] simplexTable;
    private List<Integer> basicList;
    private List<Integer> simplexBasicList;
    private int rows;
    private int cols;
    int srows;
    int scols;
    private String target;
    private SimplexImpl simplex;

    public void run(List<Function> functionList, List<Restriction> restrictionList)
    {
        init(functionList, restrictionList);
        calculate();
    }

    private void init(List<Function> functionList, List<Restriction> restrictionList)
    {
        createTable(functionList, restrictionList);
        simplex = new SimplexImpl(table, basicList, target, rows, cols);
    }

    private void createTable(List<Function> functionList, List<Restriction> restrictionList)
    {
        Utils.function = functionList.get(0);
        int s = restrictionList.size();
        rows = s + 1;
        cols = s + 3;
        srows = rows;
        scols = cols;
        target = functionList.get(0).getTarget();
        table = new float[rows][cols];
        basicList = new ArrayList<>();
        for (int i = 0; i < s; i++)
        {
            table[i][0] = restrictionList.get(i).getValue();
            basicList.add(i + 3);
        }

        table[s][1] = 0 - functionList.get(0).getX1();
        table[s][2] = 0 - functionList.get(0).getX2();

        for (int i = 0; i < rows - 1; i++)
        {
            Restriction restriction = restrictionList.get(i);
            for (int j = 1; j < cols; j++)
            {
                if (j == 1)
                    table[i][j] = restriction.getX1();
                else if (j == 2)
                    table[i][j] = restriction.getX2();
                else
                {
                    table[i][j + i] = 1;
                    break;
                }
            }
        }
        Utils.printTable(table, basicList, cols, rows);
    }

    private void calculate()
    {
        simplex.calculate(false);
        saveSimplexTable();
        for (Integer i : Arrays.asList(1,2))
        {
            System.out.println("Gomory second start for (X" + i + ")");
            boolean b = true;
            while (!isIntSolution(i))
            {
                HashMap<Integer, Float> addRestriction = createRestriction(i);
                modifyTable(addRestriction, b);
                simplex.setTable(table);
                simplex.setBlist(basicList);
                simplex.setRows(rows);
                simplex.setCols(cols);
                simplex.calculate(true);
                b = false;
            }
            System.out.println("Gomory second end for (X" + i + ")");
            Utils.printSolution(table, basicList, rows, false);
            restoreSimplextable();
        }
    }

    private void saveSimplexTable()
    {
        simplexTable = new float[srows][scols];
        for (int i = 0; i < srows; i++)
        {
            for (int j = 0; j < scols; j++)
            {
                simplexTable[i][j] = table[i][j];
            }
        }

        simplexBasicList = new ArrayList<>(basicList);
    }

    private void restoreSimplextable()
    {
        table = new float[srows][scols];
        for (int i = 0; i < srows; i++)
        {
            for (int j = 0; j < scols; j++)
            {
                table[i][j] = simplexTable[i][j];
            }
        }

        rows = srows;
        cols = scols;
        basicList = new ArrayList<>(simplexBasicList);
    }

    private void modifyTable(HashMap<Integer, Float> map, boolean b)
    {
        if (b)
        {
            for (int i = 0; i < cols; i++)
            {
                table[rows - 1][i] = 0 - table[rows - 1][i];
            }
        }

        basicList.add(cols);
        float[][] modTable = new float[rows + 1][cols + 1];
        for (int i = 0; i < rows - 1; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                modTable[i][j] = table[i][j];
            }
        }

        for (int i = 0; i < cols + 1; i++)
        {
            modTable[rows - 1][i] = map.get(i);
        }

        for (int i = 0; i < cols; i++)
        {
            modTable[rows][i] = table[rows - 1][i];
        }

        rows = rows + 1;
        cols = cols + 1;
        table = modTable;
    }

    private HashMap<Integer, Float> createRestriction(int n)
    {
        HashMap<Integer, Float> map = new HashMap<>();
        int row = basicList.indexOf(n);
        float b = table[row][0] - (int) table[row][0];
        map.put(0, b * (-1));

        for (int i = 1; i < cols; i++)
        {
            float num = table[row][i];
            int v = -1;

            if (i == n)
            {
                map.put(i, 0f);
            }
            else if (num < 0)
            {
                float q = num * (b/(b - 1));
                map.put(i, q * v);
            }
            else
            {
                float q = num;
                map.put(i, q * v);
            }
        }

        map.put(cols, 1.0f);
        return map;
    }

    private boolean isIntSolution(int num)
    {
        if (basicList.indexOf(num) == -1)
            return true;

        float d = table[basicList.indexOf(num)][0];

        if ((d - (int) d) >= 0.1 && (d - (int) d) < 0.9)
        {
            return false;
        }
        return true;
    }
}
