import model.SolutionNode;
import util.Utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

public class SimplexForBranches
{
    private float[][] table;
    private List<Integer> blist;
    private int rows;
    private int cols;
    private boolean solved;
    private String target;

    public SimplexForBranches()
    {}

    public SolutionNode calculate()
    {
        int i = 0;
        int rrow = 0;
        int rcol = 0;
        double el = 0;

        System.out.println("Simplex method start");
        System.out.println();
        Utils.printTable(table, blist, cols, rows);

        while (!solved)
        {
            if (isDualSimplex())
            {
                rrow = getResRowForDual();
                if (!checkRow(rrow))
                {
                    SolutionNode banned = new SolutionNode();
                    banned.setBanned(true);
                    banned.setNoSolution(true);
                    return banned;
                }
                rcol = getResColForDual(rrow);
            } else
            {
                rcol = getResCol();
                rrow = getResRow(rcol);
            }
            el = table[rrow][rcol];
            resolveTable(el, rrow, rcol);
            Utils.printTable(table, blist, cols, rows);
            if (++i > 20)
            {
                SolutionNode banned = new SolutionNode();
                banned.setBanned(true);
                banned.setNoSolution(true);
                return banned;
            }
        }
        System.out.println("Simplex method end");
        solved = false;
        Utils.printSolution(table, blist, rows, false);
        return new SolutionNode(table, blist);
    }

    private boolean checkRow(int row)
    {
        int cnt = 0;
        for (int i = 1; i < cols; i++)
        {
            if (table[row][i] < 0)
                ++cnt;
        }

        return cnt != 0;
    }

    private void resolveTable(double el, int rrow, int rcol)
    {
        ArrayList<Integer> oRows = fillORows(rrow);
        ArrayList<Integer> oCols = fillOCols(rcol);

        for (int row : oRows)
        {
            for (int col : oCols)
            {
                table[row][col] = new BigDecimal((table[row][col] * el - table[rrow][col] * table[row][rcol]) / el).setScale(2, RoundingMode.UP).floatValue();
            }
        }

        for (int i = 0; i < rows; i++)
        {
            if (i == rrow)
                continue;
            table[i][rcol] = new BigDecimal(table[i][rcol] / (0 - el)).setScale(2, RoundingMode.UP).floatValue();
        }

        for (int i = 0; i < cols; i++)
        {
            if (i == rcol)
                continue;
            table[rrow][i] = new BigDecimal(table[rrow][i] / el).setScale(2, RoundingMode.UP).floatValue();
        }

        table[rrow][rcol] = new BigDecimal(1 / el).setScale(2, RoundingMode.UP).floatValue();
        int ncol = blist.get(rrow);

        for (int i = 0; i < rows ; i++)
        {
            table[i][ncol] = table[i][rcol];
            if (i == rrow)
            {
                table[i][rcol] = 1;
                continue;
            }
            table[i][rcol] = 0;
        }

        blist.set(rrow, rcol);

        checkIfSolved();
    }

    private ArrayList<Integer> fillORows(int rrow)
    {
        ArrayList list = new ArrayList();
        for (int i = 0; i < rows; i++)
        {
            if (i == rrow)
                continue;
            list.add(i);
        }
        return list;
    }

    private ArrayList<Integer> fillOCols(int rcol)
    {
        ArrayList list = new ArrayList();
        for (int i = 0; i < cols; i++)
        {
            if (i == rcol)
                continue;
            list.add(i);
        }
        return list;
    }

    private void checkIfSolved()
    {
        for (int i = 0; i < cols; i++)
        {
            if (table[rows-1][i] < 0)
            {
                solved = false;
                return;
            }
        }
        solved = true;
    }

    private int getResCol()
    {
        int c = 0;
        double min = 0;
        for (int i = 0; i < cols; i++)
        {
            if (table[rows - 1][i] < 0 && table[rows - 1][i] < min)
            {
                min = table[rows - 1][i];
                c = i;
            }
        }
        return c;
    }

    private int getResRow(int rcol)
    {
        HashMap<Integer, Double> map = new HashMap<>();
        for (int i = 0; i < rows; i++)
        {
            if (table[i][rcol] > 0)
                map.put(i, new BigDecimal(table[i][0] / table[i][rcol]).setScale(2, RoundingMode.UP).doubleValue());
        }
        List<Map.Entry<Integer, Double>> list = map.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getValue)).collect(Collectors.toList());

        return list.get(0).getKey();
    }

    private int getResRowForDual()
    {
        double num = 0;
        int row = 0;
        for (int i = 0; i < rows -1; i++)
        {
            if (table[i][0] < num)
            {
                num = table[i][0];
                row = i;
            }
        }
        return row;
    }

    private int getResColForDual(int rrow)
    {
        HashMap<Integer, Double> map = new HashMap<>();
        for (int i = 1; i < cols; i++)
        {
            if (table[rows-1][i] != 0 && table[rrow][i] != 0)
            {
                map.put(i, new BigDecimal(table[rows-1][i] / table[rrow][i]).setScale(2, RoundingMode.UP).doubleValue());
            }
        }
        List<Map.Entry<Integer, Double>> list = map.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getValue)).collect(Collectors.toList());

        return list.get(0).getKey();
    }

    private boolean isDualSimplex()
    {
        for (int i = 0; i < rows; i++)
        {
            if (table[i][0] < 0)
                return true;
        }
        return false;
    }

    public void setTable(float[][] table)
    {
        this.table = new float[rows][cols];
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                this.table[i][j] = table[i][j];
            }
        }
    }

    public void setBlist(List<Integer> blist)
    {
        this.blist = new ArrayList<>(blist);
    }

    public void setRows(int rows)
    {
        this.rows = rows;
    }

    public void setCols(int cols)
    {
        this.cols = cols;
    }
}
