import util.Utils;

import java.util.*;
import java.util.stream.Collectors;

public class SimplexImpl
{
    private float[][] table;
    private List<Integer> blist;
    private int rows;
    private int cols;
    private boolean solved;
    private String target;

    public SimplexImpl() {}

    public SimplexImpl(float[][] table, List<Integer> blist, String target, int rows, int cols)
    {
        this.table = table;
        this.blist = blist;
        this.target = target;
        this.rows = rows;
        this.cols = cols;
    }

    public void calculate(boolean gomoryMode)
    {
        int rrow = 0;
        int rcol = 0;
        float el = 0;

        if (gomoryMode)
        {
            if (isDualSimplex())
            {
                rrow = getResRowForDual();
                rcol = getResColForDual(rrow);
            } else
            {
                rcol = getResCol();
                rrow = getResRow(rcol);
            }
            el = table[rrow][rcol];
            resolveTable(el, rrow, rcol, gomoryMode);
            Utils.printTable(table, blist, cols, rows);
        }
        else
        {
            System.out.println("Simplex method start");
            System.out.println();

            while (!solved)
            {
                if (isDualSimplex())
                {
                    rrow = getResRowForDual();
                    rcol = getResColForDual(rrow);
                } else
                {
                    rcol = getResCol();
                    rrow = getResRow(rcol);
                }
                el = table[rrow][rcol];
                resolveTable(el, rrow, rcol, gomoryMode);
                Utils.printTable(table, blist, cols, rows);
            }
            System.out.println("Simplex method end");
            Utils.printSolution(table, blist, rows, false);
        }
    }

    private void resolveTable(float el, int rrow, int rcol, boolean gomoryMode)
    {
        ArrayList<Integer> oRows = fillORows(rrow);
        ArrayList<Integer> oCols = fillOCols(rcol);

        for (int row : oRows)
        {
            for (int col : oCols)
            {
                table[row][col] = (table[row][col] * el - table[rrow][col] * table[row][rcol]) / el;
            }
        }

        for (int i = 0; i < rows; i++)
        {
            if (i == rrow)
                continue;
            table[i][rcol] = table[i][rcol] / (0 - el);
        }

        for (int i = 0; i < cols; i++)
        {
            if (i == rcol)
                continue;
            table[rrow][i] = table[rrow][i] / el;
        }

        table[rrow][rcol] = 1 / el;
        int ncol = blist.get(rrow);

        for (int i = 0; i < rows ; i++)
        {
            table[i][ncol] = table[i][rcol];
            if (i == rrow)
            {
                table[i][rcol] = 1;
                continue;
            }
            table[i][rcol] = 0;
        }

        blist.set(rrow, rcol);

        if (!gomoryMode)
            checkIfSolved();
    }

    private ArrayList<Integer> fillORows(int rrow)
    {
        ArrayList list = new ArrayList();
        for (int i = 0; i < rows; i++)
        {
            if (i == rrow)
                continue;
            list.add(i);
        }
        return list;
    }

    private ArrayList<Integer> fillOCols(int rcol)
    {
        ArrayList list = new ArrayList();
        for (int i = 0; i < cols; i++)
        {
            if (i == rcol)
                continue;
            list.add(i);
        }
        return list;
    }

    private void checkIfSolved()
    {
        for (int i = 0; i < cols; i++)
        {
            if (table[rows-1][i] < 0)
            {
                solved = false;
                return;
            }
        }
        solved = true;
    }

    private int getResCol()
    {
        int c = 0;
        for (int i = 0; i < cols; i++)
        {
            if (table[rows - 1][i] < 0 && table[rows - 1][i] < c)
                c = i;
        }
        return c;
    }

    private int getResRow(int rcol)
    {
        HashMap<Integer, Float> map = new HashMap<>();
        for (int i = 0; i < rows; i++)
        {
            if (table[i][rcol] > 0)
                map.put(i, table[i][0] / table[i][rcol]);
        }
        List<Map.Entry<Integer, Float>> list = map.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getValue)).collect(Collectors.toList());

        return list.get(0).getKey();
    }

    private int getResRowForDual()
    {
        float num = 0;
        int row = 0;
        for (int i = 0; i < rows -1; i++)
        {
            if (table[i][0] < num)
            {
                num = table[i][0];
                row = i;
            }
        }
        return row;
    }

    private int getResColForDual(int rrow)
    {
        HashMap<Integer, Float> map = new HashMap<>();
        for (int i = 1; i < cols; i++)
        {
            if (table[rows-1][i] != 0 && table[rrow][i] != 0)
            {
                map.put(i, table[rows-1][i] / table[rrow][i]);
            }
        }
        List<Map.Entry<Integer, Float>> list = map.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getValue)).collect(Collectors.toList());

        return list.get(0).getKey();
    }

    private boolean isDualSimplex()
    {
        for (int i = 0; i < rows; i++)
        {
            if (table[i][0] < 0)
                return true;
        }
        return false;
    }

    public void setTable(float[][] table)
    {
        this.table = table;
    }

    public void setBlist(List<Integer> blist)
    {
        this.blist = blist;
    }

    public void setRows(int rows)
    {
        this.rows = rows;
    }

    public void setCols(int cols)
    {
        this.cols = cols;
    }
}
