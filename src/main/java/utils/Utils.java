package utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Utils {

    private static int lastRowIndex(Double[][] matrix) {
        return matrix[0].length - 1;
    }



    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places+2, RoundingMode.DOWN);
        bd = new BigDecimal(bd.doubleValue());
        bd = bd.setScale(places+1, RoundingMode.UP);
        bd = new BigDecimal(bd.doubleValue());
        bd = bd.setScale(places, RoundingMode.UP);
        return bd.doubleValue();
    }

}
