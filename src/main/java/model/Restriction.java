package model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Restriction
{
    private float x1;
    private float x2;
    private String sign;
    private float value;

    public float getX1()
    {
        return x1;
    }

    public void setX1(float x1)
    {
        this.x1 = x1;
    }

    public float getX2()
    {
        return x2;
    }

    public void setX2(float x2)
    {
        this.x2 = x2;
    }

    public String getSign()
    {
        return sign;
    }

    public void setSign(String sign)
    {
        this.sign = sign;
    }

    public float getValue()
    {
        return value;
    }

    public void setValue(float value)
    {
        this.value = value;
    }
}
