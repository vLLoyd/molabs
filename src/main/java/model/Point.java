package model;

import lombok.Data;

@Data
public class Point {

    private final int row;
    private final int column;

}
