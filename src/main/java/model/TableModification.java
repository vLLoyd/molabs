package model;

import java.util.List;

public class TableModification
{
    private float[][] table;
    private int rows;
    private int cols;
    private List<Integer> basicList;

    public TableModification(float[][] table, List<Integer> basicList, int rows, int cols)
    {
        this.table = table;
        this.rows = rows;
        this.cols = cols;
        this.basicList = basicList;
    }

    public float[][] getTable()
    {
        return table;
    }

    public void setTable(float[][] table)
    {
        this.table = table;
    }

    public int getRows()
    {
        return rows;
    }

    public void setRows(int rows)
    {
        this.rows = rows;
    }

    public int getCols()
    {
        return cols;
    }

    public void setCols(int cols)
    {
        this.cols = cols;
    }

    public List<Integer> getBasicList()
    {
        return basicList;
    }

    public void setBasicList(List<Integer> basicList)
    {
        this.basicList = basicList;
    }
}
