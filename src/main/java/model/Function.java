package model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Function
{
    private float x1;
    private float x2;
    private float freeMember;
    private String target;

    public float getX1()
    {
        return x1;
    }

    public void setX1(float x1)
    {
        this.x1 = x1;
    }

    public float getX2()
    {
        return x2;
    }

    public void setX2(float x2)
    {
        this.x2 = x2;
    }

    public float getFreeMember()
    {
        return freeMember;
    }

    public void setFreeMember(float freeMember)
    {
        this.freeMember = freeMember;
    }

    public String getTarget()
    {
        return target;
    }

    public void setTarget(String target)
    {
        this.target = target;
    }
}
