package model;

import java.util.List;

public class SolutionNode
{
    public static Function FUNCTION;
    private int id;
    private SolutionNode parent;
    private List<SolutionNode> children;
    private TableModification tableModification;
    private boolean root;
    private boolean solved;
    private boolean banned;
    private boolean noSolution;
    private double x1;
    private double x2;
    private double f;

    public SolutionNode()
    {}

    public SolutionNode(float[][] table, List<Integer> basicList)
    {
        for (Integer i : basicList)
        {
            if (i == 1)
            {
                this.x1 = table[basicList.indexOf(i)][0];
            }
            if (i == 2)
            {
                this.x2 = table[basicList.indexOf(i)][0];
            }
        }

        this.f = FUNCTION.getX1() * x1 + FUNCTION.getX2() * x2 + FUNCTION.getFreeMember();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public SolutionNode getParent()
    {
        return parent;
    }

    public void setParent(SolutionNode parent)
    {
        this.parent = parent;
    }

    public List<SolutionNode> getChildren()
    {
        return children;
    }

    public void setChildren(List<SolutionNode> children)
    {
        if (this.children == null)
            this.children = children;
        else
            this.children.addAll(children);
    }

    public boolean isSolved()
    {
        return solved;
    }

    public void setSolved(boolean solved)
    {
        this.solved = solved;
    }

    public boolean isBanned()
    {
        return banned;
    }

    public void setBanned(boolean banned)
    {
        this.banned = banned;
    }

    public float[][] getTable()
    {
        return tableModification.getTable();
    }

    public void setTable(float[][] table)
    {
        this.tableModification.setTable(table);
    }

    public double getX1()
    {
        return x1;
    }

    public void setX1(double x1)
    {
        this.x1 = x1;
    }

    public double getX2()
    {
        return x2;
    }

    public void setX2(double x2)
    {
        this.x2 = x2;
    }

    public double getF()
    {
        return f;
    }

    public void setF(double f)
    {
        this.f = f;
    }

    public boolean isRoot()
    {
        return root;
    }

    public void setRoot(boolean root)
    {
        this.root = root;
    }

    public TableModification getTableModification()
    {
        return tableModification;
    }

    public void setTableModification(TableModification tableModification)
    {
        this.tableModification = tableModification;
    }

    public boolean isNoSolution()
    {
        return noSolution;
    }

    public void setNoSolution(boolean noSolution)
    {
        this.noSolution = noSolution;
    }

    @Override
    public String toString()
    {
        return "{Id = " + id + "x1 = " + x1 + "; x2 = " + x2 + "; F = " + f + ".}";
    }
}
