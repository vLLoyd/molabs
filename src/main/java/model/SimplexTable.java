package model;

import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Data
public class SimplexTable {
    private List<String> basis;
    private List<String> freeElements;
    private BigDecimal[][] matrix;

    private List<BigDecimal> simplexColumn;
    private Mode mode;

    public void addNewRestrictionRow(BigDecimal[] restrictionRow) {
        BigDecimal[][] currentMatrix = new BigDecimal[matrix.length + 1][];
        for (int i = 0; i < matrix.length; i++)
            currentMatrix[i] = matrix[i].clone();

        currentMatrix[currentMatrix.length - 2] = restrictionRow.clone();
        currentMatrix[currentMatrix.length - 1] = matrix[matrix.length - 1].clone();
        matrix = currentMatrix;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("\t1\t\t");
        for (String element : freeElements) {
            builder.append(element).append("\t\t");
        }
        builder.append("∆j")
                .append("\n");
        for (int i = 0; i <= basis.size(); i++) {
            if (i != basis.size()) {
                builder.append(basis.get(i))
                        .append("\t");
            } else {
                builder.append("f\t");
            }

            for (BigDecimal pointValue : matrix[i]) {
                builder.append(pointValue.setScale(3, RoundingMode.DOWN).setScale(1, RoundingMode.DOWN).toString());
                if (pointValue.setScale(3, RoundingMode.DOWN).setScale(1, RoundingMode.DOWN).toString().length() < 4)
                    builder.append("\t\t");
                else
                    builder.append("\t");
            }
            if (simplexColumn != null && i != simplexColumn.size())
                builder.append(simplexColumn.get(i).setScale(3, RoundingMode.DOWN).setScale(1, RoundingMode.DOWN).toString());

            builder.append("\n");
        }

        return builder.toString();
    }
}

