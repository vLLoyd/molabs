package util;

import model.Function;
import model.SolutionNode;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Utils
{
    public static Function function;


    public static void printTable(float[][] table, List<Integer> basicList, int cols, int rows)
    {
        System.out.printf("%8s", "BP");
        System.out.printf("%8s", "V");

        for (int i = 0; i < cols - 1; i++)
        {
            System.out.printf("%8s", "X" + (i + 1));
        }
        System.out.println();

        for (int i = 0; i < rows; i++)
        {
            if (i < rows - 1)
                System.out.printf("%8s", "X" + basicList.get(i));
            else
                System.out.printf("%8s", "F");

            for (int j = 0; j < cols; j++)
            {
                System.out.printf("%8.2f",table[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void printSolution(float[][] table, List<Integer> basicList, int rows, boolean b)
    {
        System.out.println("============================================");
        System.out.println("Solution: ");
        StringBuilder sb = new StringBuilder();
        float x1 = 0;
        float x2 = 0;
        for (Integer i : basicList)
        {
            if (i == 1)
            {
                x1 = table[basicList.indexOf(i)][0];
            }
            if (i == 2)
            {
                x2 = table[basicList.indexOf(i)][0];
            }
        }
        if (b)
        {
            x1 = Math.round(x1);
            x2 = Math.round(x2);
        }
        sb.append("X1 = " + String.format("%.2f", x1) + "; X2 = " + String.format("%.2f", x2) + "; ");
        float f = function.getX1() * x1 + function.getX2() * x2 + function.getFreeMember();
        sb.append("F = " + String.format("%.1f", f));
        System.out.println(sb.toString());
        System.out.println("============================================");
    }

    public static void printTree(TreeMap<Integer, SolutionNode> map)
    {
        for (Map.Entry<Integer, SolutionNode> entry : map.entrySet())
        {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
        System.out.println("============================================");
    }
}
