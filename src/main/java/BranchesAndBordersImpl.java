import model.Function;
import model.Restriction;
import util.Utils;
import model.SolutionNode;
import model.TableModification;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

public class BranchesAndBordersImpl
{
    private List<Function> functionList;
    private List<Restriction> restrictionList;
    private SimplexForBranches simplex;
    private List<SolutionNode> bufferedSolutionsList = new ArrayList<>();
    private TreeMap<Integer, SolutionNode> solutionNodeMap = new TreeMap<>();
    private double rootF;
    private boolean isExit;

    public void run(List<Function> functionList, List<Restriction> restrictionList)
    {
        init(functionList, restrictionList);
        calculate();
    }

    private void init(List<Function> functionList, List<Restriction> restrictionList)
    {
        this.functionList = functionList;
        this.restrictionList = restrictionList;
        simplex = new SimplexForBranches();
    }

    private TableModification createTable()
    {
        List<Function> functionList = this.functionList;
        SolutionNode.FUNCTION = functionList.get(0);
        Utils.function = functionList.get(0);
        List<Restriction> restrictionList = this.restrictionList;
        int s = restrictionList.size();
        int rows = s + 1;
        int cols = s + 3;
        float[][] table = new float[rows][cols];
        List<Integer> basicList = new ArrayList<>();
        for (int i = 0; i < s; i++)
        {
            table[i][0] = new BigDecimal(restrictionList.get(i).getValue()).setScale(2, RoundingMode.DOWN).floatValue();
            basicList.add(i + 3);
        }

        table[s][1] = 0 - functionList.get(0).getX1();
        table[s][2] = 0 - functionList.get(0).getX2();

        for (int i = 0; i < rows - 1; i++)
        {
            Restriction restriction = restrictionList.get(i);
            for (int j = 1; j < cols; j++)
            {
                if (j == 1)
                    table[i][j] = new BigDecimal(restriction.getX1()).setScale(2, RoundingMode.DOWN).floatValue();
                else if (j == 2)
                    table[i][j] = new BigDecimal(restriction.getX2()).setScale(2, RoundingMode.DOWN).floatValue();
                else
                {
                    table[i][j + i] = 1;
                    break;
                }
            }
        }
        Utils.printTable(table, basicList, cols, rows);
        return new TableModification(table, basicList, rows, cols);
    }

    private void calculate()
    {
        String currentId = "1";
        TableModification mod = createTable();
        simplex.setCols(mod.getCols());
        simplex.setRows(mod.getRows());
        simplex.setTable(mod.getTable());
        simplex.setBlist(mod.getBasicList());
        SolutionNode currentSolution = simplex.calculate();
        rootF = currentSolution.getF();
        currentSolution.setRoot(true);
        currentSolution.setTableModification(mod);
        currentSolution.setId(Integer.valueOf(currentId));
        solutionNodeMap.put(currentSolution.getId(), currentSolution);

        while (!isExit)
        {
            currentId = String.valueOf(currentSolution.getId());

            if (currentSolution.getX1() - (int) currentSolution.getX1() != 0)
                currentSolution.setChildren(createSolutions("X1", currentId, currentSolution));
            if (currentSolution.getX2() - (int) currentSolution.getX2() != 0)
                currentSolution.setChildren(createSolutions("X2", currentId, currentSolution));

            currentSolution.setSolved(true);
            try
            {
                currentSolution = findCurrentSolution(currentSolution, false);
            }
            catch (Exception e)
            {
                isExit = true;
            }
        }
        Utils.printTree(solutionNodeMap);
        System.out.println("Final solution: " + bufferedSolutionsList.stream().max(Comparator.comparing(SolutionNode::getF)).orElseThrow(NoSuchElementException::new));
    }

    private SolutionNode findCurrentSolution(SolutionNode currentSolution, boolean recursive) throws Exception
    {
        if (!recursive)
        {
            for (SolutionNode node : currentSolution.getChildren())
            {
                if (((node.getX1() - (int) node.getX1() < 0.1 || node.getX1() - (int) node.getX1() > 0.9) &&
                        (node.getX2() - (int) node.getX2() < 0.1 || node.getX2() - (int) node.getX2() > 0.9)) && !node.isNoSolution() && node.getF() <= rootF)
                {
                    node.setBanned(true);
                    bufferedSolutionsList.add(node);
                }
                if (((node.getX1() - (int) node.getX1() < 0.1 || node.getX1() - (int) node.getX1() > 0.9) &&
                        (node.getX2() - (int) node.getX2() < 0.1 || node.getX2() - (int) node.getX2() > 0.9)) && !node.isNoSolution() && node.getF() > rootF)
                    bufferedSolutionsList.add(node);
            }

            for (SolutionNode node : currentSolution.getChildren())
            {
                if (node.getF() == rootF || node.getF() == node.getParent().getF())
                    node.setBanned(true);
                if (!bufferedSolutionsList.isEmpty())
                {
                    for (SolutionNode buf : bufferedSolutionsList)
                    {
                        if (node.getF() < buf.getF())
                            node.setBanned(true);
                    }
                }
            }
        }

        List<SolutionNode> filteredList = currentSolution.getChildren().stream().filter(v -> !v.isBanned() && !v.isSolved())
                .collect(Collectors.toList());

        if (filteredList.isEmpty())
        {
            if (currentSolution.isRoot())
                throw new Exception("Root reached");
            else
                findCurrentSolution(currentSolution.getParent(), true);
        }

        return filteredList.stream().max(Comparator.comparing(SolutionNode::getF)).orElseThrow(NoSuchElementException::new);
    }

    private List<SolutionNode> createSolutions(String x, String id, SolutionNode currentSolution)
    {
        List<SolutionNode> list = new ArrayList<>();
        if (x.equalsIgnoreCase("X1"))
        {
            list.add(createSolution(createRestriction((int) currentSolution.getX1(), currentSolution.getTableModification().getCols(), "<=", "X1"), currentSolution, id + "1"));
            list.add(createSolution(createRestriction((int) currentSolution.getX1() + 1, currentSolution.getTableModification().getCols(),">=", "X1"), currentSolution, id + "2"));
        }
        if (x.equalsIgnoreCase("X2"))
        {
            list.add(createSolution(createRestriction((int) currentSolution.getX2(), currentSolution.getTableModification().getCols(),"<=", "X2"), currentSolution, id + "3"));
            list.add(createSolution(createRestriction((int) currentSolution.getX2() + 1, currentSolution.getTableModification().getCols(),">=", "X2"), currentSolution, id + "4"));
        }

        return list;
    }

    private SolutionNode createSolution(Map<Integer, Float> restriction, SolutionNode currentSolution, String id)
    {
        TableModification mod = modifyTable(restriction, currentSolution.getTableModification());
        simplex.setRows(mod.getRows());
        simplex.setCols(mod.getCols());
        simplex.setTable(mod.getTable());
        simplex.setBlist(mod.getBasicList());
        SolutionNode solutionNode = simplex.calculate();
        solutionNode.setTableModification(mod);
        solutionNode.setParent(currentSolution);
        solutionNode.setId(Integer.valueOf(id));
        solutionNodeMap.put(solutionNode.getId(), solutionNode);
        return solutionNode;
    }

    private TableModification modifyTable(Map<Integer, Float> map, TableModification tableModification)
    {
        int rows = tableModification.getRows();
        int cols = tableModification.getCols();

        List<Integer> list = new ArrayList<>(tableModification.getBasicList());
        list.add(cols);
        float[][] modTable = new float[rows + 1][cols + 1];
        for (int i = 0; i < rows - 1; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                modTable[i][j] = tableModification.getTable()[i][j];
            }
        }

        for (int i = 0; i < cols + 1; i++)
        {
            modTable[rows - 1][i] = map.get(i);
        }

        for (int i = 0; i < cols; i++)
        {
            modTable[rows][i] = tableModification.getTable()[rows - 1][i];
        }

        rows = rows + 1;
        cols = cols + 1;

        return new TableModification(modTable, list, rows, cols);
    }

    private Map<Integer, Float> createRestriction(float num, int cols, String sign, String var)
    {
        HashMap<Integer, Float> map = new HashMap<>();
        if (sign.equalsIgnoreCase(">="))
            map.put(0, -num);
        else
            map.put(0, num);

        for (int i = 1; i < cols; i++)
        {
            if (i == 1 && var.equalsIgnoreCase("X1"))
            {
                if (sign.equalsIgnoreCase(">="))
                    map.put(i, -1f);
                else
                    map.put(i, 1f);
                continue;
            }
            if (i == 2 && var.equalsIgnoreCase("X2"))
            {
                if (sign.equalsIgnoreCase(">="))
                    map.put(i, -1f);
                else
                    map.put(i, 1f);
                continue;
            }
            map.put(i, 0f);
        }
        map.put(cols, 1f);

        return map;
    }
}
