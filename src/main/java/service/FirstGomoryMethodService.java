package service;

import model.SimplexTable;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class FirstGomoryMethodService {

    private final SimplexTable simplexTable;
    private final SimplexMethodService simplexMethodService;
    private final List<String> basisToCheck;
    private int rowToMakeRestrictionIndex;
    private int restrictionCounter = 0;

    public FirstGomoryMethodService(SimplexTable simplexTable, List<String> basisToCheck) {
        this.simplexTable = simplexTable;
        flipLastRowSign();
        this.simplexMethodService = new SimplexMethodService(simplexTable);
        this.basisToCheck = basisToCheck;
    }

    public boolean isResolved() {
        List<Integer> basisPosition = new ArrayList<>();
        BigDecimal[][] matrix = simplexTable.getMatrix();

        for (String variable : basisToCheck) {
            if (simplexTable.getBasis().contains(variable)) {
                basisPosition.add(simplexTable.getBasis().indexOf(variable));
            }
        }

        for (Integer position : basisPosition) {
            if (matrix[position][0].abs().setScale(1, RoundingMode.HALF_UP).subtract(matrix[position][0].abs().setScale(0, RoundingMode.FLOOR)).compareTo(BigDecimal.valueOf(0.1)) >= 1) {
                return false;
            }
        }

        return true;
    }

    public void resolveFirstGomory() {
        findRowForNewRestriction();
        createAndAddNewRestriction();
        simplexMethodService.incrementLastRow();
        simplexMethodService.resolveSimplexTable(true);
    }

    private void findRowForNewRestriction() {
        BigDecimal currentValue = BigDecimal.ZERO;
        int currentPosition = -1;
        BigDecimal[][] matrix = simplexTable.getMatrix();

        for (int i = 0; i < matrix.length - 1; i++) {
            if (matrix[i][0].abs().subtract(matrix[i][0].abs().setScale(0, RoundingMode.FLOOR)).compareTo(currentValue) == 1) {
                currentValue = matrix[i][0].abs().subtract(matrix[i][0].abs().setScale(0, RoundingMode.FLOOR)).setScale(2, RoundingMode.UP);
                currentPosition = i;
            }
        }
        if (currentPosition == -1)
            throw new RuntimeException("Row mustn't be -1");

        rowToMakeRestrictionIndex = currentPosition;
        System.out.println("Row to make new restriction is " + rowToMakeRestrictionIndex);
    }

    private void createAndAddNewRestriction() {
        List<BigDecimal> newCoefficients = new ArrayList<>();
        BigDecimal[][] matrix = simplexTable.getMatrix();
        BigDecimal valueToAdd;

        for (int i = 0; i < matrix[rowToMakeRestrictionIndex].length; i++) {

            if (matrix[rowToMakeRestrictionIndex][i].compareTo(BigDecimal.ZERO) == -1) {
                valueToAdd = (matrix[rowToMakeRestrictionIndex][i].subtract(BigDecimal.valueOf((matrix[rowToMakeRestrictionIndex][i].intValue()) - 1))).negate().setScale(4, RoundingMode.UP);
            } else {
                valueToAdd = (matrix[rowToMakeRestrictionIndex][i].subtract(
                        BigDecimal.valueOf(matrix[rowToMakeRestrictionIndex][i].intValue()))).negate().setScale(4, RoundingMode.UP);
            }

            newCoefficients.add(valueToAdd);
        }

        simplexTable.addNewRestrictionRow(newCoefficients.toArray(BigDecimal[]::new));

        ArrayList<String> basisCopy = new ArrayList<>(simplexTable.getBasis());
        basisCopy.add(simplexTable.getBasis().size(), "s" + restrictionCounter);
        simplexTable.setBasis(basisCopy);

        restrictionCounter++;
        System.out.println("New simplex table with restriction:");
        System.out.println(simplexTable.toString());
    }


    private void flipLastRowSign() {
        BigDecimal[] currentLastRow = simplexTable.getMatrix()[simplexTable.getMatrix().length - 1].clone();
        BigDecimal[] newLastRow = new BigDecimal[currentLastRow.length];

        for (int i = 0; i < currentLastRow.length; i++) {
            newLastRow[i] = currentLastRow[i].negate();
        }

        simplexTable.getMatrix()[simplexTable.getMatrix().length - 1] = newLastRow;
    }
}
