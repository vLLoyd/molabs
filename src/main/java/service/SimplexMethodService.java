package service;

import model.Mode;
import model.Point;
import model.SimplexTable;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;

public class SimplexMethodService {

    private final SimplexTable simplexTable;
    private int resolvingColumnIndex;
    private int resolvingRowIndex;
    private boolean isDualSimplex;
    private Point resolvingPointCoordinates;

    private int lastRowIndex;

    public SimplexMethodService(SimplexTable simplexTable) {
        this.simplexTable = simplexTable;
        this.lastRowIndex = simplexTable.getMatrix().length - 1;
    }

    public void incrementLastRow() {
        lastRowIndex++;
    }

    public boolean isResolved() {
        checkIfDualSimplex();
        if (!isDualSimplex) {
            BigDecimal[] lastRow = simplexTable.getMatrix()[lastRowIndex];

            for (BigDecimal aDouble : lastRow) {
                if (aDouble.compareTo(BigDecimal.ZERO) < 0)
                    return false;
            }
            return true;
        } else {
            for (int i = 0; i < simplexTable.getMatrix().length - 1; i++) {
                BigDecimal currentElement = simplexTable.getMatrix()[i][0];
                if (currentElement.compareTo(BigDecimal.ZERO) < 0)
                    return false;
            }
            return true;
        }
    }

    public void resolveSimplexTable(boolean isGomoryMethod) {
        countResolvingPoint(isGomoryMethod);
        System.out.println("Resolving point coordinates are " + resolvingPointCoordinates + ", the value is "
                + simplexTable.getMatrix()[resolvingRowIndex][resolvingColumnIndex]);
        System.out.println("Current simplex table");
        System.out.println(simplexTable.toString());

        BigDecimal[][] matrixToOverwrite = new BigDecimal[simplexTable.getMatrix().length][];
        for (int i = 0; i < simplexTable.getMatrix().length; i++)
            matrixToOverwrite[i] = simplexTable.getMatrix()[i].clone();

        for (int i = 0; i < matrixToOverwrite.length; i++) {
            for (int j = 0; j < matrixToOverwrite[i].length; j++) {
                matrixToOverwrite[i][j] = recountPoint(i, j);
            }
        }

        simplexTable.setMatrix(matrixToOverwrite);
        resolveNewBasisAndFreeElements();
        simplexTable.setSimplexColumn(null);
        System.out.println("New simplex table");
        System.out.println(simplexTable.toString());
    }

    private void resolveNewBasisAndFreeElements() {
        String currentBasisValue = simplexTable.getBasis().get(resolvingRowIndex);
        String currentFreeElementValue = simplexTable.getFreeElements().get(resolvingColumnIndex - 1);

        simplexTable.getBasis().set(resolvingRowIndex, currentFreeElementValue);
        simplexTable.getFreeElements().set(resolvingColumnIndex - 1, currentBasisValue);
    }

    private BigDecimal recountPoint(int row, int column) {
        BigDecimal[][] matrix = simplexTable.getMatrix();
        BigDecimal resolvingPointValue = matrix[resolvingPointCoordinates.getRow()][resolvingPointCoordinates.getColumn()];
        if (row == resolvingRowIndex && column == resolvingColumnIndex) {
            return BigDecimal.ONE.divide(matrix[row][column], 1, RoundingMode.HALF_EVEN);
        }
        BigDecimal currentPointValue = matrix[row][column];
        if (row == resolvingRowIndex) {
            return currentPointValue.divide(resolvingPointValue, 1, RoundingMode.HALF_EVEN);
        }
        if (column == resolvingColumnIndex) {
            return currentPointValue.divide(resolvingPointValue.negate(), 1, RoundingMode.HALF_EVEN);
        }
        return (resolvingPointValue.multiply(currentPointValue)
                .subtract(matrix[resolvingRowIndex][column].multiply(matrix[row][resolvingColumnIndex])))
                .divide(resolvingPointValue, 1, RoundingMode.HALF_EVEN);
    }

    private void countResolvingPoint(boolean isGomoryMethod) {

        if (isGomoryMethod) {
            isDualSimplex = true;
        } else {
            checkIfDualSimplex();
        }
        if (isDualSimplex) {
            countResolvingRowDual();
            countResolvingColumnDual();
        } else {
            countResolvingColumn();
            countResolvingRow();
        }

        System.out.println("Resolving row is " + resolvingRowIndex);
        System.out.println("Resolving column is " + resolvingColumnIndex);
        resolvingPointCoordinates = new Point(resolvingRowIndex, resolvingColumnIndex);
    }

    private void checkIfDualSimplex() {
        for (int i = 0; i < simplexTable.getMatrix().length - 1; i++) {
            if (simplexTable.getMatrix()[i][0].compareTo(BigDecimal.ZERO) < 0) {
                isDualSimplex = true;
                System.out.println("\nThe problem will be solved with dual simplex method");
                return;
            }
        }
        System.out.println("\nThe problem will be solved with common simplex method");
        isDualSimplex = false;
    }

    private void countResolvingColumn() {
        BigDecimal currentValue = BigDecimal.ZERO;
        int currentIndex = -1;
        BigDecimal[] lastRow = simplexTable.getMatrix()[lastRowIndex];

        if (simplexTable.getMode() == Mode.MAX) {
            for (int i = 1; i < lastRow.length; i++) {
                if (lastRow[i].abs().compareTo(currentValue) > 0 && lastRow[i].compareTo(BigDecimal.ZERO) < 0) {
                    currentValue = lastRow[i].abs();
                    currentIndex = i;
                }
            }
        } else if (simplexTable.getMode() == Mode.MIN) {
            for (int i = 1; i < lastRow.length; i++) {
                if (lastRow[i].abs().compareTo(currentValue) > 0 && lastRow[i].compareTo(BigDecimal.ZERO) > 0) {
                    currentValue = lastRow[i].abs();
                    currentIndex = i;
                }
            }
        }
        if (currentIndex == -1)
            throw new RuntimeException("Resolving column index = -1, method failed\n");

        resolvingColumnIndex = currentIndex;
    }

    private void countSimplexColumn() {
        ArrayList<BigDecimal> simplexColumn = new ArrayList<>();

        for (int i = 0; i < simplexTable.getMatrix().length - 1; i++) {
            BigDecimal[] row = simplexTable.getMatrix()[i];
            simplexColumn.add(row[0].divide(row[resolvingColumnIndex], 1, RoundingMode.HALF_EVEN));
        }

        simplexTable.setSimplexColumn(simplexColumn);
    }

    private void countResolvingRow() {
        countSimplexColumn();
        resolvingRowIndex = simplexTable.getSimplexColumn().indexOf(
                simplexTable.getSimplexColumn().stream()
                        .filter(it -> it.compareTo(BigDecimal.ZERO) > 0)
                        .min(Comparator.comparing(BigDecimal::doubleValue))
                        .orElse(BigDecimal.ZERO)
        );
    }

    private void countResolvingRowDual() {
        BigDecimal value = BigDecimal.ZERO;
        for (int i = 0; i < simplexTable.getMatrix().length - 1; i++) {
            if (simplexTable.getMatrix()[i][0].abs().compareTo(value) > 0 && simplexTable.getMatrix()[i][0].compareTo(BigDecimal.ZERO) < 0) {
                value = simplexTable.getMatrix()[i][0].abs();
                resolvingRowIndex = i;
            }
        }
    }

    private void countResolvingColumnDual() {
        BigDecimal value = BigDecimal.ZERO;
        for (int i = 1; i < simplexTable.getMatrix()[resolvingRowIndex].length; i++) {
            if (simplexTable.getMatrix()[resolvingRowIndex][i].compareTo(BigDecimal.ZERO) < 0) {
                if (simplexTable.getMatrix()[lastRowIndex][i].divide(simplexTable.getMatrix()[resolvingRowIndex][i], 2, RoundingMode.HALF_UP).abs().compareTo(value) == -1 || value.compareTo(BigDecimal.ZERO) == 0) {
                    value = simplexTable.getMatrix()[lastRowIndex][i].divide(simplexTable.getMatrix()[resolvingRowIndex][i], 2, RoundingMode.HALF_UP).abs();
                    resolvingColumnIndex = i;
                }
            }
        }
    }
}
