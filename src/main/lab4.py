import numpy as np
import pandas as pd
from copy import deepcopy
import matplotlib.pyplot as plt
import pylab
import matplotlib.ticker
import random

class table:
    """ Класс, описывающий симплекс-таблицу
    """

    def __init__(self, A, b, c_param, basis):
        """ Конструктор класса
        Атрибуты:
            A -- Матрица коэффициентов A = a(ij) системы уравнений
            b -- Правая часть ограничений равенства
            c -- Матрица значений целевой функции
            c_param -- Матрица значений целевой функции в параметрическом виде
            table -- таблица для вывода
            basis -- базис
        """
        self.A = A
        self.b = b
        self.c = []
        self.c_param = c_param
        self.table = []
        self.basis = basis

    def define_c(self, t):
        """ Метод класса инициализации таблицы при заданном параметре t
        input:
            t -- параметр
        """
        self.c = []
        for elem in self.c_param:
            self.c += [elem['k'] * t + elem['free']]

    def print_param_table(self):
        """ Метод класса для вывода параметрической симплекс-таблицы
        """
        last_row = []
        for elem in self.c_param:
            if elem['k'] < 0:
                if elem['free'] == 0:
                    s = str(elem['k']) + "*t"
                else:
                    s = str(elem['free']) + str(elem['k']) + "*t"
            elif elem['k'] > 0:
                if elem['free'] == 0:
                    s = str(elem['k']) + "*t"
                else:
                    s = str(elem['free']) + '+' + str(elem['k']) + "*t"

            elif elem['k'] == 0:
                if elem['free'] == 0:
                    s = "0"
                else:
                    s = str(elem['free'])
            last_row.append(s)
        table = deepcopy(self.A)
        for i in range(len(self.b)):
            table[i] += [self.b[i]]
        df = pd.DataFrame(table)
        df.rename(index=lambda x: "x" + str(self.basis[x] + 1), inplace=True)
        df = df.T
        df['F'] = last_row
        df = df.T
        last_key = "x" + str(len(table[1]))
        df.rename(columns=lambda x: "x" + str(x + 1), inplace=True)
        df.rename(columns={last_key: "B"}, inplace=True)
        print(df)

    def print_table(self):
        """ Метод класса для вывода симплекс-таблицы
        """
        table = deepcopy(self.A)
        for i in range(len(self.b)):
            table[i] += [self.b[i]]
        df = pd.DataFrame(table)
        df.rename(index=lambda x: "x" + str(self.basis[x] + 1), inplace=True)
        df = df.T
        df['F'] = self.c
        df = df.T
        last_key = "x" + str(len(table[1]))
        df.rename(columns=lambda x: "x" + str(x + 1), inplace=True)
        df.rename(columns={last_key: "B"}, inplace=True)
        print(df)


def computing(p, MainCol, MainRow, EnableElem):
    """ Функция расчета симплекс-таблицы
    input:
        p -- объект класса 'table'
        MainCol -- размещающий столбец
        MainRow -- разрешающая строка
        EnableElem -- разрешающий элемент
    output:
        A -- Матрица коэффициентов A = a(ij) системы уравнений
        b -- Правая часть ограничений равенства
        c_param -- Матрица значений целевой функции в параметрическом виде
    """
    new_table = []
    A, b, c = [], [], []
    nol = {'k': 0., 'free': 0.}
    c_param = []
    table = deepcopy(p.A)
    for i in range(len(p.b)):
        table[i] += [p.b[i]]
    table.append(p.c)

    j = 0
    for row in table[:-1]:
        new_row = []
        for i in range(len(row)):
            if j == MainRow:
                new_row.append(np.round(row[i] / EnableElem, 2))
            else:
                new_row.append(np.round(row[i] - (table[MainRow][i] * table[j][MainCol]) / EnableElem, 2))
        j += 1
        new_table.append(new_row)
    for row in new_table:
        A += [row[:-1]]
        b += [np.round(row[-1], 2)]
    c = new_table[-1]

    for i in range(len(p.A[1]) + 1):
        elem = deepcopy(nol)
        if i != MainCol:
            elem['k'] = np.round(p.c_param[i]['k'] - (p.c_param[MainCol]['k'] * table[MainRow][i]) / EnableElem, 2)
            elem['free'] = np.round(
                p.c_param[i]['free'] - (p.c_param[MainCol]['free'] * table[MainRow][i]) / EnableElem, 2)
        c_param += [elem]
    return A, b, c_param


def my_simplex(p1):
    """ Функция определения разрешающих строки, столбца
        и элемента для симплекс-метода
    input:
        p1 -- объект класса 'table'
    output:
        p2 -- обновленный объект класса 'table' после симплекс-метода
    """
    #    разрешающий столбец
    MainCol = p1.c[:-1].index(min(p1.c[:-1]))

    #    разрешающая строка
    BperXi = []
    for i in range(len(p1.b)):
        if p1.A[i][MainCol] == 0:
            BperXi.append(0.)
        else:
            BperXi.append(p1.b[i] / p1.A[i][MainCol])
    MainRow = BperXi.index(min([i for i in BperXi if i > 0]))

    #    разрещающий элемент
    EnableElem = p1.A[MainRow][MainCol]
    print("Разрешающий элемент:", EnableElem, "[", MainRow + 1, ",", MainCol + 1, "]")

    #   базис
    basis = deepcopy(p1.basis)
    basis[MainRow] = MainCol
    A, b, c_param = computing(p1, MainCol, MainRow, EnableElem)
    p2 = table(A, b, c_param, basis)
    if (0 in basis) and (1 in basis):
        x1 = b[basis.index(0)]
        x2 = b[basis.index(1)]
    elif (0 not in basis) and (1 in basis):
        x1 = 0.
        x2 = b[basis.index(1)]
    elif (0 in basis) and (1 not in basis):
        x1 = b[basis.index(0)]
        x2 = 0.
    return p2, x1, x2


def borders(cc):
    """ Функция определения ограничений параметра t,
    при которых найденное решение остается оптимальным планом
    input:
        cс -- Матрица значений целевой функции в параметрическом виде
    output:
        borders -- ограничивающий интервал
    """
    global start, end
    borders = {"start": start, "end": end}
    for elem in cc:
        if elem['k'] != 0:
            dot = -elem['free'] / elem['k']
            if elem['k'] > 0:
                if dot >= borders["start"]:
                    borders["start"] = dot
            elif elem['k'] < 0:
                if dot <= borders["end"]:
                    borders["end"] = dot
    return borders


def print_OP(c_param):
    """ Функция вывода оптимального плана в параметрическом виде
    input:
        cс -- Матрица значений целевой функции в параметрическом виде
    output:
        last_row -- список значений коэффициентов целевой функции в параметрическом виде
        F -- значение функции в параметрическом виде
    """
    global c_0_free
    last_row = []
    for elem in c_param[:-1]:
        if elem['k'] < 0:
            if elem['free'] == 0:
                s = str(elem['k']) + "*t"
            else:
                s = str(elem['free']) + str(elem['k']) + "*t"
        elif elem['k'] > 0:
            if elem['free'] == 0:
                s = str(elem['k']) + "*t"
            else:
                s = str(elem['free']) + '+' + str(elem['k']) + "*t"

        elif elem['k'] == 0:
            if elem['free'] == 0:
                s = "0"
            else:
                s = str(elem['free'])
        last_row.append(s)

        elem = c_param[-1]
        if elem['k'] < 0:
            if elem['free'] == 0:
                F = str(elem['k']) + "*t+(" + str(- c_0_free) + ")"
            else:
                F = str(elem['free']) + str(elem['k']) + "*t"
        elif elem['k'] > 0:
            if elem['free'] == 0:
                F = str(elem['k']) + "*t"
            else:
                F = str(elem['free']) + '+' + str(elem['k']) + "*t"

        elif elem['k'] == 0:
            if elem['free'] == 0:
                F = "0"
            else:
                F = str(elem['free'] + c_0_free)
    return last_row, F


def go_simplex():
    """ Функция запуска симплекс-метода
    """
    global start, end, count_out, x1_graph, x2_graph

    p1 = table(A_0, b_0, c_0, basis_0)

    p1.print_param_table()
    if start == -np.inf:
        if end == np.inf:
            p1.define_c(1.)
        else:
            p1.define_c(end)
    else:
        p1.define_c(start)
    cond = True

    i = 2
    optimal = True
    print("Симплекс-метод начат")
    print('Итерация: 1')
    p2, x1, x2 = my_simplex(p1)

    ent = float(input("Введите t: "))

    while not (ent >= start and ent <= end):
        print("Введите t = [", start, ';', end, ']')
        ent = float(input())
    else:
        p2.print_param_table()
        p2.define_c(ent)
        p2.print_table()
        cc = p2.c[:-1]
        while cond:
            if (any(x < 0 for x in cc)):

                print('Итерация: ', i)
                i += 1
                p2, x1, x2 = my_simplex(p2)
                p2.print_param_table()
                print("При t=", ent)
                p2.define_c(ent)
                p2.print_table()
                cc = p2.c
                if i > 10:
                    print("Решение не оптимально")
                    optimal = False
                    break
            else:
                cond = False
        if optimal:
            print("Симплекс-метод закончен")
            OP, F = print_OP(p2.c_param)
            print("Оптимальный план: X =", OP)
            print("X1=", x1)
            print("X2", x2)
            x1_graph[count_out] = x1
            x2_graph [count_out]= x2
            print("Fmax =", F)
            F_t[count_out] = F
            bb = borders(p2.c_param)
            print("при t = [", bb['start'], ';', bb['end'], ']')
            print(" ")
            param_t[count_out] = round(bb['start'],2)
            count_out += 1
            #print("PARAMETR=", param_t)
            if bb['end'] < end:
                go_simplex()
            else:

                return 0


# Инициализация
print("Параметрический анализ задач линейного программирования")
print(" ")
print("____________________________________________________________________")
print(" ")
print("Сколько точек в ограничении?")
k = int(input())
x_first = [None]*(k+1)
y_first = [None]*(k+1)
i = 0
print("Введите координаты точек ОДР последовательно")
while i < k:
    print("Введите координату ", i+1, "точки через Enter")
    x_first[i] = float(input())
    y_first[i] = float(input())
    i += 1

print("Координаты исследуемой функции")
j = 0
f1 = [None]*2
f2 = [None]*2
while j < 2:
    print("Введите координату №", j+1, "фунции")
    f1[j] = float(input())
    f2[j] = float(input())
    j += 1
print("Введите исследуемый промежуток параметра t")
start = int(input())
end = int(input())

x1_in_var = [None] * (k - 1)
x2_in_var = [None] * (k - 1)

b_0 = [0]*(k-1)
c = [0]*3
count = 0
while count < (k - 1):
    x1_in_var[count] = round(y_first[count] - y_first[count + 1], 2)
    x2_in_var[count] = round(x_first[count + 1] - x_first[count], 2)
    b_0[count] = round((x_first[count + 1] - x_first[count]) * y_first[count] - (y_first[count + 1] - y_first[count]) * x_first[count], 2)
    count += 1
i = 0
c[0] = "t-2"
c[1] = round(-(f1[i + 1] - f1[i]), 0)
c[2] = round(-((f2[i + 1] - f2[i]) * f1[i] - (f1[i + 1] - f1[i]) * f2[i]), 0)

print("Исследуемая функция: F(x1,x2): (", c[0],") * x1 + (",-c[1],") * x2",-c[2]," --> max")
print("Ограничения:")
while i < (k-1):
    print("       ", x1_in_var[i],"* x1 + (",x2_in_var[i],") * x2","<=",b_0[i])
    i += 1
print(" ")
print("____________________________________________________________________")
print(" ")
# Матрица коэффициентов A = a(ij) системы уравнений
A_0 = [[0] * (k + 1) for i in range(k - 1)]

i = 0
j = 0
while i < (k-1):
    if j == 0:
        A_0[i][j] = x1_in_var[i]
        i += 1
        if i == (k - 1):
            i = 0
            j = 1
            while i < (k - 1):
                A_0[i][j] = x2_in_var[i]
                i += 1
            i = 0
            while j < k:
                A_0[i][j+1] = 1
                i += 1
                j += 1




p0 = {"k": -1., "free": 2}
p1 = {"k": 0., "free": c[1]}

p2 = {"k": 0., "free": 0.}
p3 = {"k": 0., "free": 0.}
p4 = {"k": 0., "free": 0.}
p5 = {"k": 0., "free": c[2]}
# Матрица значений целевой функции в параметрическом виде
c_0 = [p0, p1, p2, p3, p4, p5]
c_0_free = 0
eps = 0.1



F_t = [None]*2
param_t = [None]*3
param_t[2] = end

count_out = 0
x1_graph = [None]*4
x2_graph = [None]*4
# Определение изначального базиса
basis_0 = []
for i in range(2, len(A_0[1])):
    basis_0.append(i)
go_simplex()

print(" ")
print("____________________________________________________________________")
print(" ")
#print("X1 for Graph=", x1_graph)
#print("X2 for Graph=", x2_graph)
i = 0
while i < 2:
    print("При t = [", param_t[i], ",", param_t[i+1], "] Fmax = ", F_t[i])
    i += 1
#print("THE END")

rand_t = [None]*2
free_c = [None]*2
i = 0
r = 0
while i < 2:
    r = random.uniform(param_t[i], param_t[i+1])
    rand_t[i] = round(r, 2)
    i += 1
#print("RANDOM T=", rand_t)
i = 0
while i < 2:
    free_c[i] = round((-(rand_t[i]-2)*x1_graph[i]+(c[1])*x2_graph[i]), 2)
    i += 1

fig = plt.figure()
axes = fig.add_subplot(111)
plt.xlabel('x1')
plt.ylabel('x2')
plt.grid(True)
locator = matplotlib.ticker.MultipleLocator (base=1)
axes.xaxis.set_major_locator(locator)
locator2 = matplotlib.ticker.MultipleLocator (base=1)
axes.yaxis.set_major_locator(locator2)
#если в ограничении присутствует точка (0,0), то этот блок открываем
x_first[k] = 0
y_first[k] = 0

plt.scatter(x_first, y_first)
plt.plot(x_first, y_first)
plt.scatter(f1, f2)
plt.fill(x_first, y_first, 'gray')
func_1 = np.linspace(-1, 5, 100)
func_2 = lambda func_1: (-2 * func_1 + c[2])/(-c[1])
plt.plot(func_1, func_2(func_1),"r--")




func_x1 = np.linspace(2, 6, 100)
func_1y_max = lambda func_x1: ((rand_t[0]-2) * func_x1 + free_c[0])/c[1]
func_x2 = np.linspace(6, 10, 100)
func_2y_max = lambda func_x2: ((rand_t[1]-2) * func_x2 + free_c[1])/c[1]
#func_x3 = np.linspace(8, 13, 100)
#func_3y_max = lambda func_x3: ((rand_t[2]-2) * func_x3 + free_c[2])/c[1]
#func_x4 = np.linspace(14, 16, 100)
#func_4y_max = lambda func_x4: ((rand_t[3]-2) * func_x4 + free_c[3])/c[1]
plt.scatter(x1_graph, x2_graph)

plt.plot(func_x1, func_1y_max(func_x1))
plt.plot(func_x2, func_2y_max(func_x2))
#plt.plot(func_x3, func_3y_max(func_x3))
#plt.plot(func_x4, func_4y_max(func_x4))
plt.legend(("permissible range", "F(x1,x2) at t=0", "Fmax at t["+str(param_t[0])+";"+str(param_t[1])+"]",
            "Fmax at t["+str(param_t[1])+";"+str(param_t[2])+"]"))
#            "Fmax at t["+str(param_t[2])+";"+str(param_t[3])+"]",
#            "Fmax at t["+str(param_t[3])+";"+str(param_t[4])+"]"))
plt.show()


